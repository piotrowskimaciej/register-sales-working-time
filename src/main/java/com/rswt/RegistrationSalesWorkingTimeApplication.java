package com.rswt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationSalesWorkingTimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegistrationSalesWorkingTimeApplication.class, args);
    }
}
