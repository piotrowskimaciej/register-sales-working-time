package com.rswt.repository.worktime;

import com.rswt.entity.WorkTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface WorkTimeRepository extends JpaRepository<WorkTime, Long> {

    Optional<WorkTime> findById(Long id);

    Optional<WorkTime> findByEmployee_IdAndWorkStartBetween(Long id, LocalDateTime min, LocalDateTime max);

    Collection<WorkTime> findByEmployee_Id(Long id);

    Collection<WorkTime> findAllByEmployee_IdAndWorkStartBetween(Long id, LocalDateTime min, LocalDateTime max);
}
