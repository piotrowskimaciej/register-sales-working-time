package com.rswt.repository.user;

import com.rswt.entity.User;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.stereotype.Repository;

        import java.util.*;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameOrEmail(String username, String email);

    @Query(
            value = "select * from user_model u where u.id not in   \n" +
                    "(select u.id from user_model u inner join user_roles r on (u.id = r.user_id) where roles = 'ROLE_MANAGER' or roles = 'ROLE_ADMIN')",
            nativeQuery = true)
    Collection<User> findAllEmployees();
}
