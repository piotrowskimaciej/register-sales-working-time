package com.rswt.data.dto;

import java.time.LocalDateTime;

public class SaleDTO {

    private Long id;

    private LocalDateTime saleDate;

    private int amount;

    private ProductDTO product;

    public SaleDTO() {}

    public SaleDTO(Long id, LocalDateTime saleDate, int amount) {
        this.id = id;
        this.saleDate = saleDate;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(LocalDateTime saleDate) {
        this.saleDate = saleDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }
}
