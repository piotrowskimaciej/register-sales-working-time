package com.rswt.data.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class WorkTimeDTO {

    private Long id;

    private LocalDateTime workStart;

    private LocalDateTime workEnd;

    private String comment;

    private Set<SaleDTO> sale;

    public WorkTimeDTO() {
        this.sale = new HashSet<>();
    }

    public WorkTimeDTO(Long id, LocalDateTime workStart, LocalDateTime workEnd, String comment) {
        this.id = id;
        this.workStart = workStart;
        this.workEnd = workEnd;
        this.comment = comment;
        this.sale = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWorkStart() {
        return workStart;
    }

    public void setWorkStart(LocalDateTime workStart) {
        this.workStart = workStart;
    }

    public LocalDateTime getWorkEnd() {
        return workEnd;
    }

    public void setWorkEnd(LocalDateTime workEnd) {
        this.workEnd = workEnd;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<SaleDTO> getSale() {
        return sale;
    }

    public void setSale(Set<SaleDTO> sale) {
        this.sale = sale;
    }
}
