package com.rswt.data.dto;

public class ProductDTO {

    private Long id;

    private String productname;

    private String type;

    public ProductDTO() {}

    public ProductDTO(Long id, String productname, String type) {
        this.id = id;
        this.productname = productname;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
