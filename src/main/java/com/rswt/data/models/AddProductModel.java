package com.rswt.data.models;

import javax.validation.constraints.NotNull;

public class AddProductModel {

    @NotNull
    private String productname;

    @NotNull
    private String type;

    public AddProductModel() {}

    public AddProductModel(@NotNull String productname, @NotNull String type) {
        this.productname = productname;
        this.type = type;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
