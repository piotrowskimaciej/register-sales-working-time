package com.rswt.data.models;

import javax.validation.constraints.NotNull;

public class EndWorkModel {

    @NotNull
    Long wtId;

    @NotNull
    String wtComment;

    public EndWorkModel() {}

    public EndWorkModel(Long wtId, String wtComment) {
        this.wtId = wtId;
        this.wtComment = wtComment;
    }

    public Long getWtId() {
        return wtId;
    }

    public void setWtId(Long wtId) {
        this.wtId = wtId;
    }

    public String getWtComment() {
        return wtComment;
    }

    public void setWtComment(String wtComment) {
        this.wtComment = wtComment;
    }
}
