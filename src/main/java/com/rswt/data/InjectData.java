package com.rswt.data;

import com.rswt.config.security.UserRole;
import com.rswt.entity.Product;
import com.rswt.entity.Sale;
import com.rswt.entity.User;
import com.rswt.entity.WorkTime;
import com.rswt.repository.product.ProductRepository;
import com.rswt.repository.sale.SaleRepository;
import com.rswt.repository.user.UserRepository;
import com.rswt.repository.worktime.WorkTimeRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
public class InjectData {

    private UserRepository userRepository;
    private ProductRepository productRepository;
    private SaleRepository saleRepository;
    private WorkTimeRepository worktimeRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private User employer;
    private User employer2;
    private User manager;
    private User admin;
    private Product computer;
    private Product printer;
    private Product mouse;
    private Sale saleOne;
    private Sale saleTwo;
    private Sale saleThree;
    private Sale saleFour;
    private WorkTime worktime;

    public InjectData(UserRepository userRepository, ProductRepository productRepository, SaleRepository saleRepository,
                      WorkTimeRepository worktimeRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.saleRepository = saleRepository;
        this.worktimeRepository = worktimeRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostConstruct
    private void inject() {
        injectUsers();
        injectProducts();
        injectWorkTime();
        injectSale();
    }

    private void injectUsers() {
        this.employer = new User("employee", "John", "Smith", "employer@mail.com", bCryptPasswordEncoder.encode("password"));
        this.employer.addRole(UserRole.ROLE_EMPLOYEE);
        this.employer2 = new User("employee2", "William ", "Johnson", "employer2@mail.com", bCryptPasswordEncoder.encode("password"));
        this.employer2.addRole(UserRole.ROLE_EMPLOYEE);
        this.manager = new User("manager", "Phil", "Jones","manager@mail.com", bCryptPasswordEncoder.encode("password"));
        this.manager.addRole(UserRole.ROLE_MANAGER);
        this.manager.addRole(UserRole.ROLE_EMPLOYEE);
        this.admin = new User("admin", "Adam", "Admin", "admin@mail.com", bCryptPasswordEncoder.encode("admin"));
        this.admin.addRole(UserRole.ROLE_ADMIN);
        this.admin.addRole(UserRole.ROLE_EMPLOYEE);
        this.admin.addRole(UserRole.ROLE_MANAGER);
        userRepository.save(employer);
        userRepository.save(employer2);
        userRepository.save(manager);
        userRepository.save(admin);
    }

    private void injectProducts() {
        this.computer = new Product("Laptop", "Computers");
        this.printer = new Product("Printer", "Printers");
        this.mouse = new Product("Mouse", "Accessories");
        productRepository.save(computer);
        productRepository.save(printer);
        productRepository.save(mouse);
    }

    private void injectSale() {
        this.saleOne = new Sale(LocalDateTime.of(2017,12,21,14,30), 1);
        saleRepository.save(saleOne);

        this.saleOne.setProduct(computer);
        this.mouse.getProductSale().add(saleOne);
        this.saleOne.setSoldBy(worktime);
        this.worktime.getSale().add(saleOne);
        saleRepository.save(saleOne);

        this.saleTwo = new Sale(LocalDateTime.of(2017,12,21,15,50), 1);
        saleRepository.save(saleTwo);

        this.saleTwo.setProduct(mouse);
        this.mouse.getProductSale().add(saleTwo);
        this.saleTwo.setSoldBy(worktime);
        this.worktime.getSale().add(saleTwo);
        saleRepository.save(saleTwo);

        this.saleThree = new Sale(LocalDateTime.of(2017,12,21,10,20), 1);
        saleRepository.save(saleThree);

        this.saleThree.setProduct(printer);
        this.printer.getProductSale().add(saleThree);
        this.saleThree.setSoldBy(worktime);
        this.worktime.getSale().add(saleThree);
        saleRepository.save(saleThree);
    }

    private void injectWorkTime() {
        this.worktime = new WorkTime(LocalDateTime.of(2017,12,21,10,00),
                LocalDateTime.of(2017,12,21,20,00),"2 sold product");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,1,8,10,00),
                LocalDateTime.of(2018,1,8,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,3,1,10,00),
                LocalDateTime.of(2018,3,1,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,2,1,10,00),
                LocalDateTime.of(2018,2,1,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,4,6,10,00),
                LocalDateTime.of(2018,4,6,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,11,8,10,00),
                LocalDateTime.of(2018,11,8,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);

        this.worktime = new WorkTime(LocalDateTime.of(2018,6,25,10,00),
                LocalDateTime.of(2018,6,25,20,00),"No sold products");
        worktimeRepository.save(worktime);
        this.worktime.setEmployee(employer);
        worktimeRepository.save(worktime);
    }
}
