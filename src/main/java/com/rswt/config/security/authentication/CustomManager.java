package com.rswt.config.security.authentication;

import com.rswt.config.security.LoginModel;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomManager implements AuthenticationManager {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        var principal = authentication.getPrincipal().toString();
        var credentials = authentication.getCredentials().toString();
        var authorities = authentication.getAuthorities();

        LoginModel loginModel = new LoginModel(principal, credentials);

        return new UsernamePasswordAuthenticationToken(loginModel.getLogin(), loginModel.getPassword(),
                authorities);
    }
}
