package com.rswt.config.security.authentication;

import com.rswt.entity.User;
import com.rswt.repository.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomService implements UserDetailsService {

    private UserRepository userRepository;

    public CustomService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsernameOrEmail(username, username);
        if (!user.isPresent()) throw new UsernameNotFoundException("");

        return user.get();
    }
}
