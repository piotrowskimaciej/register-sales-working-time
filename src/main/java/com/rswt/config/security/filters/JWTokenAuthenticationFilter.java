package com.rswt.config.security.filters;

import com.rswt.config.security.token.JWTokenAuthenticationService;
import com.rswt.config.security.token.JWTokenParser;
import com.rswt.repository.user.UserRepository;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class JWTokenAuthenticationFilter extends OncePerRequestFilter {

    private final String HEADER_STRING = "Authorization";
    private JWTokenAuthenticationService jwTokenAuthenticationService;
    private UserRepository userRepository;

    public JWTokenAuthenticationFilter(JWTokenAuthenticationService jwTokenAuthenticationService, UserRepository userRepository) {
        this.jwTokenAuthenticationService = jwTokenAuthenticationService;
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        try {
            if (isUserExist(httpServletRequest)) {
                Authentication authentication = jwTokenAuthenticationService.getAuthentication(httpServletRequest);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (ExpiredJwtException ex) {
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private boolean isUserExist(HttpServletRequest httpServletRequest) throws ExpiredJwtException {
        if (Optional.ofNullable(httpServletRequest.getHeader(HEADER_STRING)).isPresent()) {
            String username = JWTokenParser.getLoginFromToken(httpServletRequest.getHeader(HEADER_STRING));
            return userRepository.findByUsername(username).isPresent();
        }
        return false;
    }
}
