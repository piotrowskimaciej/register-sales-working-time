package com.rswt.config.security;


public enum UserRole {

    ROLE_EMPLOYEE, ROLE_MANAGER, ROLE_ADMIN
}
