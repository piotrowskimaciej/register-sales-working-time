package com.rswt.config.security.token;

import io.jsonwebtoken.JwtException;
import javassist.NotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class JWTokenAuthenticationService {

    private final String AUTHORIZATION_HEADER = "Authorization";

    public Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION_HEADER);
        if (token != null) {
            try {
                return buildAuthentication(token);
            } catch (JwtException | NotFoundException ex) {
                return null;
            }
        }
        return null;
    }

    private Authentication buildAuthentication(String token) throws NotFoundException {
        String username = JWTokenParser.getLoginFromToken(token);
        Collection<GrantedAuthority> authorities = JWTokenParser.getAuthoritiesFromToken(token)
                .stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
        return new UsernamePasswordAuthenticationToken(username, null, authorities);
    }
}
