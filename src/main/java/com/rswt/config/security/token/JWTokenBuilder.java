package com.rswt.config.security.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;

public class JWTokenBuilder {

    private final String SECRET = "ThisIsASecret";
    private JWToken jwToken;

    public String buildJWToken(UserDetails userDetails) {
        builder()
                .setTokenSubject(userDetails.getUsername())
                .addAdditionalPayloadValues("role", userDetails.getAuthorities());
        return build();
    }

    public JWToken builder() {
        jwToken = new JWToken();
        return jwToken;
    }

    public String build() {
        Claims claims = buildClaims(jwToken);

        String TOKEN_HEADER_TEMPLATE = "Bearer %s";
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(this.jwToken.getTokenExpirationTime())
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        return String.format(TOKEN_HEADER_TEMPLATE, token);
    }

    private Claims buildClaims(JWToken token) {
        Claims claims = Jwts.claims().setSubject(token.getTokenSubject());
        token.getAdditionalPayloadValues().forEach(claims::put);
        return claims;
    }
}
