package com.rswt.config.security.token;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class JWToken {

    private String tokenSubject;
    private int tokenExpirationTime;
    private Map<String, Object> additionalPayloadValues;

    public JWToken() {
        this.tokenExpirationTime = 72800; //2 days
        additionalPayloadValues = new HashMap<>();
    }

    public String getTokenSubject() {
        return tokenSubject;
    }

    public JWToken setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
        return this;
    }

    public Date getTokenExpirationTime() {
        return java.sql.Timestamp.valueOf(LocalDateTime.now().plusSeconds(tokenExpirationTime));
    }

    public JWToken setTokenExpirationTime(int tokenExpirationTime) {
        this.tokenExpirationTime = tokenExpirationTime;
        return this;
    }

    public Map<String, Object> getAdditionalPayloadValues() {
        return additionalPayloadValues;
    }

    public JWToken addAdditionalPayloadValues(String key, Object value) {
        this.additionalPayloadValues.put(key, value);
        return this;
    }

    public JWToken cleanAdditionalPayloadValues() {
        this.additionalPayloadValues.clear();
        return this;
    }
}
