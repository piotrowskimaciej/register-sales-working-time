package com.rswt.config.security.token;

import com.rswt.config.security.UserRole;
import com.rswt.exceptions.NotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class JWTokenParser {

    private final String SECRET = "ThisIsASecret";
    private final String TOKEN_PREFIX = "Bearer";
    private final String AUTHORIZATION_HEADER = "Authorization";
    private static final JWTokenParser tokenParser = new JWTokenParser();

    private Claims parseClaims(String token) throws ExpiredJwtException, MalformedJwtException {
        return Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();
    }

    public static String getLoginFromToken(String token) throws NotFoundException, ExpiredJwtException {
        try {
            String username = tokenParser.parseClaims(token).getSubject();
            if (!Optional.ofNullable(username).isPresent() || username.isEmpty())
                throw new NotFoundException("Username was not found!");
            return username;
        } catch (MalformedJwtException ex) {
            return null;
        }
    }

    public static Collection<String> getAuthoritiesFromToken(String token) {
        return tokenParser.generateAuthorities(tokenParser.parseClaims(token).get("role").toString());
    }

    private Collection<String> generateAuthorities(String role) {
        var roles = new HashSet<String>();
        if (role.contains(UserRole.ROLE_EMPLOYEE.name())) {
            roles.add(UserRole.ROLE_EMPLOYEE.name());
        }
        if (role.contains(UserRole.ROLE_MANAGER.name())) {
            roles.add(UserRole.ROLE_MANAGER.name());
        }
        if (role.contains(UserRole.ROLE_ADMIN.name())) {
            roles.add(UserRole.ROLE_ADMIN.name());
        }
        return roles;
    }
}
