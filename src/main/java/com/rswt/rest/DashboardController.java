package com.rswt.rest;

import com.rswt.data.dto.ProductDTO;
import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.data.models.EndWorkModel;
import com.rswt.services.DashboardService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class DashboardController {

    private DashboardService dashboardService;

    public DashboardController(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @PostMapping("/getWorkTime")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<WorkTimeDTO> getWorkTime() {
        return new ResponseEntity<>(dashboardService.getWorkTime(), HttpStatus.OK);
    }

    @PostMapping("/startWork")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<Long> startWork() {
        return new ResponseEntity<>(dashboardService.startWork(), HttpStatus.OK);
    }

    @PostMapping("/endWork")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<String> endWork(@RequestBody EndWorkModel endWorkModel) {
        return dashboardService.endWork(endWorkModel);
    }

    @PostMapping("/getAllProducts")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<Set<ProductDTO>> getAllProducts() {
        return new ResponseEntity<>(dashboardService.getAllProducts(), HttpStatus.OK);
    }

    @PostMapping("/saleProduct")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<String> saleProduct(@RequestParam Long pId, @RequestParam Long wId) {
        return new ResponseEntity<>(dashboardService.saleProduct(pId, wId), HttpStatus.OK);
    }
}
