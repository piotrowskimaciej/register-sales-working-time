package com.rswt.rest;

import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.services.SalesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SalesController {

    private SalesService salesService;

    public SalesController(SalesService salesService) {
        this.salesService = salesService;
    }

    @PostMapping("/getWorkTimeSales")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<WorkTimeDTO> getWorkTimeSales(@RequestParam Long id) {
        return new ResponseEntity<>(salesService.getWorkTimeSales(id), HttpStatus.OK);
    }
}
