package com.rswt.rest;

import com.rswt.data.dto.UserDTO;
import com.rswt.entity.User;
import com.rswt.services.UserPanelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserPanelController {

    private UserPanelService userPanelService;

    public UserPanelController(UserPanelService userPanelService) {
        this.userPanelService = userPanelService;
    }

    @PostMapping("/userpanel")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<UserDTO> userDetails() {
        return new ResponseEntity<>(userPanelService.getUserDetails(), HttpStatus.OK);
    }
}
