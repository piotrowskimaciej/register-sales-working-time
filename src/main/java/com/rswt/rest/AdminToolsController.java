package com.rswt.rest;

import com.rswt.data.models.AddProductModel;
import com.rswt.data.models.AddUserModel;
import com.rswt.services.AdminToolsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminToolsController {

    private AdminToolsService adminToolsService;

    public AdminToolsController(AdminToolsService adminToolsService) {
        this.adminToolsService = adminToolsService;
    }

    @PostMapping("/addNewUser")
    public ResponseEntity<String> addNewUser(@RequestBody AddUserModel addUserModel) {
        return adminToolsService.addNewUser(addUserModel);
    }

    @PostMapping("/addNewProduct")
    public ResponseEntity<String> addNewProduct(@RequestBody AddProductModel addProductModel) {
        return adminToolsService.addNewProduct(addProductModel);
    }
}
