package com.rswt.rest;

import com.rswt.config.security.LoginModel;
import com.rswt.services.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class LoginController {

    private LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginModel loginModel) {
        return loginService.login(loginModel);
    }

    @PostMapping("/userDetails")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<String> getUserName() {
        return new ResponseEntity<>(loginService.getUserName(), HttpStatus.OK);
    }

    @GetMapping("/getRoles")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public ResponseEntity<Set<String>> getRoles() {
        return new ResponseEntity<>(loginService.getRoles(), HttpStatus.OK);
    }

    @GetMapping("/manager")
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    public Boolean isManager() {
        return true;
    }

    @GetMapping("/employee")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public Boolean isEmployee() {
        return true;
    }
}
