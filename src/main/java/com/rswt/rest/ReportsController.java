package com.rswt.rest;

import com.rswt.data.dto.UserDTO;
import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.services.ReportsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class ReportsController {

    private ReportsService reportsService;

    public ReportsController(ReportsService reportsService) {
        this.reportsService = reportsService;
    }

    @PostMapping("/getAllEmployees")
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    public ResponseEntity<Set<UserDTO>> getAllEmployees() {
        return new ResponseEntity<>(reportsService.getAllEmployees(), HttpStatus.OK);
    }

    @PostMapping("/getEmployeeWorkTime")
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    public ResponseEntity<Set<WorkTimeDTO>> getEmployeeWorkTime(@RequestParam Long id, @RequestParam Long days) {
        return new ResponseEntity<>(reportsService.getEmployeeWorkTime(id, days), HttpStatus.OK);
    }
}
