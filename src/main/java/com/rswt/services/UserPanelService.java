package com.rswt.services;

import com.rswt.data.dto.UserDTO;
import com.rswt.exceptions.NotFoundException;
import com.rswt.repository.user.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserPanelService {

    private UserRepository userRepository;

    public UserPanelService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO getUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        var userDetails = new UserDTO();
        var user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            throw new NotFoundException(username);
        } else {
            var u = user.get();
            userDetails.setId(u.getId());
            userDetails.setUsername(u.getUsername());
            userDetails.setName(u.getName());
            userDetails.setSurname(u.getSurname());
            userDetails.setEmail(u.getEmail());
            return userDetails;
        }
    }
}
