package com.rswt.services;

import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.exceptions.NotFoundException;
import com.rswt.repository.worktime.WorkTimeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class SalesService {

    private WorkTimeRepository workTimeRepository;
    private ModelMapper modelMapper;

    public SalesService(WorkTimeRepository workTimeRepository, ModelMapper modelMapper) {
        this.workTimeRepository = workTimeRepository;
        this.modelMapper = modelMapper;
    }

    public WorkTimeDTO getWorkTimeSales(Long id) {
        var workTime = workTimeRepository.findById(id);

        if(!workTime.isPresent()) throw new NotFoundException("Work time not exists!");
        var wt = workTime.get();
        return modelMapper.map(wt, WorkTimeDTO.class);
    }
}
