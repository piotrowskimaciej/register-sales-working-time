package com.rswt.services;

import com.rswt.config.security.UserRole;
import com.rswt.data.models.AddProductModel;
import com.rswt.data.models.AddUserModel;
import com.rswt.entity.Product;
import com.rswt.entity.User;
import com.rswt.exceptions.ObjectAlreadyExistsException;
import com.rswt.repository.product.ProductRepository;
import com.rswt.repository.user.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminToolsService {

    private UserRepository userRepository;
    private ProductRepository productRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AdminToolsService(UserRepository userRepository, ProductRepository productRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public ResponseEntity<String> addNewUser(AddUserModel addUserModel) {
        if (userRepository.findByUsername(addUserModel.getLogin()).isPresent()) {
            throw new ObjectAlreadyExistsException("This user exists in database");
        } else {
            User addNewUser = new User();
            addNewUser.setName(addUserModel.getName());
            addNewUser.setSurname(addUserModel.getSurname());
            addNewUser.setEmail(addUserModel.getEmail());
            addNewUser.setUsername(addUserModel.getLogin());
            addNewUser.setPassword(bCryptPasswordEncoder.encode(addUserModel.getPassword()));

            if (!Optional.ofNullable(addUserModel.getRole()).isPresent())
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            else {
                if (addUserModel.getRole().equals("employee")) {
                    addNewUser.addRole(UserRole.ROLE_EMPLOYEE);
                } else {
                    addNewUser.addRole(UserRole.ROLE_EMPLOYEE);
                    addNewUser.addRole(UserRole.ROLE_MANAGER);
                }
            }
            userRepository.save(addNewUser);
        }

        return new ResponseEntity<>("employee added", HttpStatus.OK);
    }

    public ResponseEntity<String> addNewProduct(AddProductModel addProductModel) {
        if (productRepository.findByProductname(addProductModel.getProductname()).isPresent()) {
            throw new ObjectAlreadyExistsException("This product exists in database");
        } else {
            Product addNewProduct = new Product();
            addNewProduct.setProductname(addProductModel.getProductname());
            if (!Optional.ofNullable(addProductModel.getType()).isPresent())
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            else
                addNewProduct.setType(addProductModel.getType());
            productRepository.save(addNewProduct);
        }

        return new ResponseEntity<>("product added", HttpStatus.OK);
    }
}
