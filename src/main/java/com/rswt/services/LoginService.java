package com.rswt.services;

import com.rswt.config.security.LoginModel;
import com.rswt.config.security.token.JWTokenBuilder;
import com.rswt.exceptions.NotFoundException;
import com.rswt.repository.user.UserRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class LoginService {

    private AuthenticationManager customManager;
    private PasswordEncoder encoder;
    private UserDetailsService customService;
    private UserRepository userRepository;

    public LoginService(UserDetailsService customService, UserRepository userRepository,
                        PasswordEncoder encoder, AuthenticationManager customManager) {
        this.customService = customService;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.customManager = customManager;
    }

    public ResponseEntity<String> login(LoginModel loginModel) {
        var user = customService.loadUserByUsername(loginModel.getLogin());
        if (!Optional.ofNullable(loginModel.getPassword()).isPresent())
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        if (!encoder.matches(loginModel.getPassword(), user.getPassword()))
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

        var authentication = customManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginModel.getLogin(), loginModel.getPassword(), user.getAuthorities()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        var headers = new HttpHeaders();
        headers.set("token", new JWTokenBuilder().buildJWToken(user));
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

    public String getUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        var user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            throw new NotFoundException(username);
        } else {
            String name = user.get().getName();
            String surname = user.get().getSurname();
            return name + " " + surname;
        }
    }

    public Set<String> getRoles() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        var user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            throw new NotFoundException(username);
        } else {
            return user.get().getRoles();
        }
    }
}
