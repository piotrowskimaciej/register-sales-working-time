package com.rswt.services;

import com.rswt.data.dto.ProductDTO;
import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.data.models.EndWorkModel;
import com.rswt.entity.Sale;
import com.rswt.entity.WorkTime;
import com.rswt.exceptions.NotFoundException;
import com.rswt.repository.product.ProductRepository;
import com.rswt.repository.sale.SaleRepository;
import com.rswt.repository.user.UserRepository;
import com.rswt.repository.worktime.WorkTimeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    private UserRepository userRepository;
    private WorkTimeRepository workTimeRepository;
    private ProductRepository productRepository;
    private SaleRepository saleRepository;
    private ModelMapper modelMapper;

    public DashboardService(UserRepository userRepository, WorkTimeRepository workTimeRepository, ProductRepository productRepository, SaleRepository saleRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.workTimeRepository = workTimeRepository;
        this.productRepository = productRepository;
        this.saleRepository = saleRepository;
        this.modelMapper = modelMapper;
    }

    public WorkTimeDTO getWorkTime() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        var todaymin = LocalDateTime.now().with(LocalTime.MIN);
        var todaymax = LocalDateTime.now().with(LocalTime.MAX);

        var workTimeData = new WorkTimeDTO();
        var user = userRepository.findByUsername(username);

        if (!user.isPresent()) throw new NotFoundException(username);
        var uid = user.get().getId();
        var workTime = workTimeRepository.findByEmployee_IdAndWorkStartBetween(uid, todaymin, todaymax);

        if(!workTime.isPresent()) throw new NotFoundException("WorkTime not exists!");
        var wt = workTime.get();

        workTimeData.setId(wt.getId());
        workTimeData.setWorkStart(wt.getWorkStart());
        workTimeData.setWorkEnd(wt.getWorkEnd());
        workTimeData.setComment(wt.getComment());

        return workTimeData;
    }

    public Long startWork() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        var today = LocalDateTime.now();
        var user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            throw new NotFoundException(username);
        } else {
            var u = user.get();

            WorkTime createWorkTime = new WorkTime();
            createWorkTime.setWorkStart(today);
            createWorkTime.setWorkEnd(null);
            createWorkTime.setComment(null);
            createWorkTime.setEmployee(u);
            createWorkTime.setSale(null);

            workTimeRepository.save(createWorkTime);

            return createWorkTime.getId();
        }
    }

    public ResponseEntity<String> endWork(EndWorkModel endWorkModel) {
        var today = LocalDateTime.now();
        var workTime = workTimeRepository.findById(endWorkModel.getWtId());

        if (!workTime.isPresent()) {
            throw new NotFoundException("Work time not exist!");
        } else {
            var wt = workTime.get();
            wt.setWorkEnd(today);
            wt.setComment(endWorkModel.getWtComment());
            workTimeRepository.save(wt);
        }

        return new ResponseEntity<>("work finished",HttpStatus.OK);
    }

    public Set<ProductDTO> getAllProducts() {
        return productRepository.findAll()
                .stream().map(p -> modelMapper.map(p, ProductDTO.class))
                .collect(Collectors.toSet());
    }

    public String saleProduct(Long pId, Long wId) {
        var today = LocalDateTime.now();

        if (!Optional.ofNullable(wId).isPresent()) throw new NotFoundException("This Work time not exist!");
        else if (!Optional.ofNullable(pId).isPresent()) throw new NotFoundException("This Product not exist!");
        else {
            var actualWorktime = workTimeRepository.findById(wId);
            var actualProduct = productRepository.findById(pId);

            if (!actualWorktime.isPresent()) throw new NotFoundException("This Work time not exist!");
            else if (!actualProduct.isPresent()) throw new NotFoundException("This Product not exist!");
            else {
                Sale createSale = new Sale();
                createSale.setSaleDate(today);
                createSale.setAmount(1);
                createSale.setSoldBy(actualWorktime.get());
                createSale.setProduct(actualProduct.get());

                saleRepository.save(createSale);
                return "sale created";
            }
        }
    }
}
