package com.rswt.services;

import com.rswt.data.dto.UserDTO;
import com.rswt.data.dto.WorkTimeDTO;
import com.rswt.repository.user.UserRepository;
import com.rswt.repository.worktime.WorkTimeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ReportsService {

    private UserRepository userRepository;
    private WorkTimeRepository workTimeRepository;
    private ModelMapper modelMapper;

    public ReportsService(UserRepository userRepository, WorkTimeRepository workTimeRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.workTimeRepository = workTimeRepository;
        this.modelMapper = modelMapper;
    }

    public Set<UserDTO> getAllEmployees() {
        return userRepository.findAllEmployees()
                .stream().map(p -> modelMapper.map(p, UserDTO.class))
                .collect(Collectors.toSet());
    }

    public Set<WorkTimeDTO> getEmployeeWorkTime(Long id, Long days) {
        var today = LocalDateTime.now().with(LocalTime.MAX);

        if (days == 0) {
            return workTimeRepository.findByEmployee_Id(id)
                    .stream().map(p -> modelMapper.map(p, WorkTimeDTO.class))
                    .collect(Collectors.toSet());
        } else {
            var minusTenDays = today.minusDays(days);

            return workTimeRepository.findAllByEmployee_IdAndWorkStartBetween(id, minusTenDays, today)
                    .stream().map(p -> modelMapper.map(p, WorkTimeDTO.class))
                    .collect(Collectors.toSet());
        }
    }
}
