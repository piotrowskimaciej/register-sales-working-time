package com.rswt.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "product_model")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String productname;

    private String type;

    @OneToMany(mappedBy = "product")
    private Set<Sale> productSale;

    public Product() {
        this.productSale = new HashSet<>();
    }

    public Product(String productname, String type) {
        this.productname = productname;
        this.type = type;
        this.productSale = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public String getProductname() {
        return productname;
    }

    public String getType() {
        return type;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Sale> getProductSale() {
        return productSale;
    }

    public void setProductSale(Set<Sale> productSale) {
        this.productSale = productSale;
    }
}
