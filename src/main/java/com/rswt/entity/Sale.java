package com.rswt.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "sale_model")
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime saleDate;

    private int amount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "worktime_id")
    private WorkTime soldBy;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    public Sale() {}

    public Sale(LocalDateTime saleDate, int amount) {
        this.saleDate = saleDate;
        this.amount = amount;
    }

    public Sale(LocalDateTime saleDate, int amount, WorkTime soldBy, Product product) {
        this.saleDate = saleDate;
        this.amount = amount;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(LocalDateTime saleDate) {
        this.saleDate = saleDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public WorkTime getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(WorkTime soldBy) {
        this.soldBy = soldBy;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
