package com.rswt.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "worktime_model")
public class WorkTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime workStart;

    private LocalDateTime workEnd;

    private String comment;

    @OneToMany(mappedBy = "soldBy")
    private Set<Sale> sale;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")
    private User employee;

    public WorkTime() {}

    public WorkTime(LocalDateTime workStart, LocalDateTime workEnd, String comment) {
        this.workStart = workStart;
        this.workEnd = workEnd;
        this.comment = comment;
        this.sale = new HashSet<>();
    }

    public WorkTime(LocalDateTime workStart, LocalDateTime workEnd, String comment, User employee) {
        this.workStart = workStart;
        this.workEnd = workEnd;
        this.comment = comment;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWorkStart() {
        return workStart;
    }

    public void setWorkStart(LocalDateTime workStart) {
        this.workStart = workStart;
    }

    public LocalDateTime getWorkEnd() {
        return workEnd;
    }

    public void setWorkEnd(LocalDateTime workEnd) {
        this.workEnd = workEnd;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }

    public Set<Sale> getSale() {
        return sale;
    }

    public void setSale(Set<Sale> sale) {
        this.sale = sale;
    }
}
